package com.ilomatechnology.quotes.buddha.adapter;


import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.content.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.ilomatechnology.quotes.buddha.R;
import com.ilomatechnology.quotes.buddha.model.QuotesModel;

import java.util.ArrayList;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.MyViewHolder> {
    Context context;
    ArrayList<QuotesModel>list;

    public QuotesAdapter(Context context ,  ArrayList<QuotesModel>list) {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(ArrayList<QuotesModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuotesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quotes_item_list,viewGroup,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuotesAdapter.MyViewHolder myViewHolder, final int position) {

        QuotesModel model = list.get(position);

        myViewHolder.copyText.setText(model.getQuote());

        myViewHolder.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        list.get(position).getQuote());
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);
            }
        });

        myViewHolder.imgCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(myViewHolder.copyText.getText().toString(), myViewHolder.copyText.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
       return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgShare,imgCopy;
        private TextView copyText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
           imgShare =itemView.findViewById(R.id.share_imageView);
           imgCopy = itemView.findViewById(R.id.copy_imageView);
           copyText = itemView.findViewById(R.id.quotes_textView);

        }


    }
}
