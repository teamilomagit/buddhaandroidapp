package com.ilomatechnology.quotes.buddha.model;

public class QuotesModel {
    String id;
    String quote;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
