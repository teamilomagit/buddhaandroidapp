package com.ilomatechnology.quotes.buddha.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.ilomatechnology.quotes.buddha.R;
import com.ilomatechnology.quotes.buddha.adapter.QuotesAdapter;
import com.ilomatechnology.quotes.buddha.model.QuotesModel;
import com.ilomatechnology.quotes.buddha.utils.QuotesJsonManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class QuotesActivity extends AppCompatActivity {

    RecyclerView myRecyclerView;
    QuotesAdapter quotesAdapter;
    Context context;
    private AdView adView;
    private InterstitialAd mInterstitialAd;
    ArrayList<QuotesModel>list;

    private RecyclerView.LayoutManager layoutManager;
    ImageView back_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);

        loadAds();
        init();
    }

    private void loadAds() {
        MobileAds.initialize(this, "ca-app-pub-7842443993349666~1765431130");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7842443993349666/7958232568");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                finish();
            }

        });
    }

    private void init() {
        context =this;
        list = new ArrayList<>();
        myRecyclerView = findViewById(R.id.quotes_recycler_view);
        back_image = findViewById(R.id.back_imageView);
        setOnClick();

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        setAdapter();
        insertOfflineFilesFromIndexJson();

    }

    private void setOnClick() {
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Toast.makeText(QuotesActivity.this, "The interstitial wasn't loaded yet.", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });
    }


    private void setAdapter() {
        quotesAdapter = new QuotesAdapter(this , new ArrayList<QuotesModel>());
        layoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(layoutManager);
        myRecyclerView.setAdapter(quotesAdapter);
    }


    private void insertOfflineFilesFromIndexJson() {

        new QuotesJsonManager().insertDataFromJson(this, "quotes.json", QuotesModel.class,
                new QuotesJsonManager.QuotesJsonManagerInterface() {
            @Override
            public void onSuccess(Object resultObj) {

               list = (ArrayList<QuotesModel>) resultObj;

                runOnUiThread(new Runnable() {
                    public void run() {
                        quotesAdapter.updateAdapter(list);

                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Toast.makeText(QuotesActivity.this, "The interstitial wasn't loaded yet.", Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
