package com.ilomatechnology.quotes.buddha.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;
import com.ilomatechnology.quotes.buddha.R;
import com.ilomatechnology.quotes.buddha.activity.StoriesActivity;
import com.ilomatechnology.quotes.buddha.activity.StoryDetailActivity;

import java.util.ArrayList;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.MyViewHolder> {
    private String[] storyName;
    private Context context;





    public StoriesAdapter(Context context, String[] storyName){
        this.context = context;
        this.storyName = storyName ;


    }

    @NonNull
    @Override
    public StoriesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stories_item_list,viewGroup,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StoriesAdapter.MyViewHolder myViewHolder, final int position) {
        myViewHolder.storyName.setText(storyName[position]);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, StoryDetailActivity.class).putExtra("position",position+1));

            }
        });

    }


    @Override
    public int getItemCount() {
        return storyName.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView storyName;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            storyName = itemView.findViewById(R.id.storyName_textView);

        }



    }

}
