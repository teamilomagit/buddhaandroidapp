package com.ilomatechnology.quotes.buddha.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class QuotesJsonManager {




    public interface QuotesJsonManagerInterface {
        public void onSuccess(Object resultObj);

        public void onError(String error);
    }



    public void insertDataFromJson(final Context context, final String fileName,  final Class classType, final QuotesJsonManagerInterface listener) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringBuffer sb = new StringBuffer();
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                    String temp;
                    while ((temp = br.readLine()) != null)
                        sb.append(temp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String response = sb.toString();

                if (response != null && !response.isEmpty()) {
                    Gson gson = new Gson();
                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = new JSONArray(jsonObject.getString("items"));
                        ArrayList arrList = new ArrayList();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Object model = gson.fromJson(object.toString(), classType);
                            arrList.add(model);
                        }

                        if (listener != null) {
                            listener.onSuccess(arrList);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return null;
            }
        }.execute();
    }
}
