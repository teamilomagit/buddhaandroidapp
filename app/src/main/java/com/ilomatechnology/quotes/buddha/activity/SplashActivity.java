package com.ilomatechnology.quotes.buddha.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ilomatechnology.quotes.buddha.BuildConfig;
import com.ilomatechnology.quotes.buddha.R;

public class SplashActivity extends AppCompatActivity {
    Context context;
    TextView versionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

       init();
    }

    private void init() {

        String version= BuildConfig.VERSION_NAME;
        versionTextView = findViewById(R.id.version_info_textView);
        versionTextView.setText("Version : "+version);

    }

}
