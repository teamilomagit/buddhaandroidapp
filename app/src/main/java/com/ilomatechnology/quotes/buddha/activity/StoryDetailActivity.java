package com.ilomatechnology.quotes.buddha.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.ilomatechnology.quotes.buddha.R;

public class StoryDetailActivity extends AppCompatActivity {

    ImageView back_imageView;
    WebView webView;
    int pos;
    private AdView adView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);

        init();
    }

    private void init() {

        back_imageView = findViewById(R.id.back_imageView);
        webView = findViewById(R.id.storyWebView);
        adView = findViewById(R.id.adView);
        loadAds();
        setData();
        setOnClick();
    }

    private void setOnClick() {
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Toast.makeText(StoryDetailActivity.this, "The interstitial wasn't loaded yet.", Toast.LENGTH_SHORT).show();
                }
                finish();

            }
        });
    }

    private void setData() {

        pos = getIntent().getIntExtra("position", 0);
        String storyName = "story" + pos + ".html";
        webView.loadUrl("file:///android_asset/story/" + storyName);
    }

    private void loadAds() {

        MobileAds.initialize(this, "ca-app-pub-7842443993349666~1765431130");

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7842443993349666/7958232568");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                finish();
            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Toast.makeText(this, "The interstitial wasn't loaded yet.", Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
