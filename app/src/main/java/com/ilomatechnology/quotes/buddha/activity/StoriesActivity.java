package com.ilomatechnology.quotes.buddha.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.ilomatechnology.quotes.buddha.R;
import com.ilomatechnology.quotes.buddha.adapter.StoriesAdapter;

public class StoriesActivity extends AppCompatActivity {

    private Context context;
    private ImageView back_image;
    private StoriesAdapter mAdapter;
    private AdView adView;

    String[] storyName = {"The Wild Geese.","The Buddha and the Wealthy Brahmin.","The Sacrifice of the Brahmin.","Angulimala","Buddha and the Beggar","Free bird","The Art of Playing Flute","The Prophecy","The Discovery of Suffering","Story about Death"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);
        init();
    }
    private void init() {
        context = this;
        back_image = findViewById(R.id.back_imageView);
        adView = findViewById(R.id.adView);
        loadAds();
        setOnClick();
        setRecyclerView();
    }

    private void loadAds() {
        MobileAds.initialize(this, "ca-app-pub-7842443993349666~1765431130");
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void setRecyclerView() {
        RecyclerView myRecyclerView = findViewById(R.id.quotes_recycler_view);;
        mAdapter = new StoriesAdapter(context, storyName);
        RecyclerView.LayoutManager myLayoutManager = new LinearLayoutManager(context);
        myRecyclerView.setLayoutManager(myLayoutManager);
        myRecyclerView.setAdapter(mAdapter);

    }

    private void setOnClick() {
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
